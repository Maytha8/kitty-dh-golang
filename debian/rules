#!/usr/bin/make -f
# vim: noexpandtab shiftwidth=4 softtabstop=0

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

include /usr/share/dpkg/default.mk

OVERRIDE_CFLAGS = $(CFLAGS)
OVERRIDE_CPPFLAGS = $(CPPFLAGS)
OVERRIDE_LDFLAGS = $(LDFLAGS)
export OVERRIDE_CFLAGS OVERRIDE_CPPFLAGS OVERRIDE_LDFLAGS

V=
ifeq (,$(filter terse,$(DEB_BUILD_OPTIONS)))
	V=--verbose
endif

HOME = $(CURDIR)/debian/fakehome
KITTY_RUNTIME_DIRECTORY = $(CURDIR)/debian/fakeruntime

SRCDIR = $(CURDIR)

GOPATH = $(CURDIR)/_build
GO_BUILDDIR = $(GOPATH)/src
GO111MODULE = off
export GOPATH GO111MODULE
# export DH_GOLANG_INSTALL_EXTRA := gen-go-code.py terminfo shell-integration tools/unicode_names/names.txt
# export DH_GOLANG_BUILDPKG := kitty/tools/cmd
export DH_GOLANG_INSTALL_ALL := 1

%:
	dh $@ --with python3 --with sphinxdoc

override_dh_auto_clean:
	python3 setup.py $(V) clean
	dh_auto_clean --buildsystem=golang -O--builddirectory=_build

override_dh_auto_configure:
	dh_auto_configure -O--with=python3 -O--with=sphinxdoc
	dh_auto_configure -O--buildsystem=golang -O--builddirectory=_build

override_dh_auto_build:
	env HOME="$(HOME)" PATH=$$PATH:$(CURDIR)/debian/bin python3 setup.py $(V) linux-package
	cp $(CURDIR)/kitty/docs_ref_map_generated.h $(GO_BUILDDIR)/kitty/kitty
	cp $(CURDIR)/kitty/uniforms_generated.h $(GO_BUILDDIR)/kitty/kitty
	cd $(GO_BUILDDIR)/kitty && \
		$(CURDIR)/linux-package/bin/kitty +launch gen-go-code.py && \
		cd $(SRCDIR)
	# $(CURDIR)
	env GOPATH="$(GOPATH)" go build -v -o $(CURDIR)/linux-package/bin/kitten kitty/tools/cmd

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	mkdir -p "$(HOME)"
	mkdir -p "$(KITTY_RUNTIME_DIRECTORY)"
	cd $(GO_BUILDDIR)/kitty && \
		env HOME="$(HOME)" KITTY_RUNTIME_DIRECTORY="$(KITTY_RUNTIME_DIRECTORY)" python3 setup.py $(V) build-launcher && \
		env GOPATH="$(GOPATH)" HOME="$(HOME)" KITTY_RUNTIME_DIRECTORY="$(KITTY_RUNTIME_DIRECTORY)" LC_ALL=C.UTF-8 python3 setup.py $(V) test
	cd $(SRCDIR)
endif

override_dh_auto_install:
	mkdir -p $(CURDIR)/debian/tmp/usr
	cp -a linux-package/* $(CURDIR)/debian/tmp/usr/

override_dh_install:
	dh_install -Nkitty-shell-integration -Xlib/kitty/shell-integration
	dh_install -pkitty-shell-integration

override_dh_installdocs:
	dh_installdocs
	# Remove PayPal's tracking pixel
	[ ! -d debian/kitty-doc ] || sed -i '/pixel\.gif/d' debian/kitty-doc/usr/share/doc/kitty/html/support.html

override_dh_python3:
	dh_python3 --no-ext-rename

override_dh_installchangelogs:
	dh_installchangelogs docs/changelog.rst

override_dh_compress:
	dh_compress -X/html
